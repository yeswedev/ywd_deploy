# Outil de déploiement Yes We Dev

## Installation

Depuis la racine d’un projet git :
```
git submodule add https://framagit.org/yeswedev/ywd_deploy.git ywd_deploy
git submodule update --init --recursive ywd_deploy
```

## Mise-à-jour

Depuis la racine du projet :
```
git submodule update --recursive --remote ywd_deploy
```

## Configuration

Depuis la racine du projet :
```
cp ywd_deploy/toolbox.conf.example ywd_deploy/toolbox.conf
```
Puis éditez le fichier ywd_deploy/toolbox.conf pour y renseigner les informations relatives à votre projet.
```bash
DEBUG='false' # mode verbeux

PROJECT_NAME='myproject'         # nom du projet (format machine)
PROJECT_TECH='unknown'           # technologie utilisée par le projet (ex : 'laravel', 'wordpress', etc.)

#SERVER_ENVIRONMENT='production'                                        # environnement du projet (auto-détecté si cette ligne est commentée)
PROJECT_PATH="$(dirname "$(readlink --canonicalize-existing "$0")")/.." # chemin absolu vers le répertoire du projet (la valeur par défaut le définit automatiquement)
BACKUP_PATH="$HOME/yeswedev/backups/$PROJECT_NAME"                      # chemin absolu du répertoire dans lequel effectuer les sauvegardes

# Actions
DB_BACKUP='true'                         # sauvegarder un dump de la base de données avant toute action de déploiement
DB_BACKUP_CLEANUP_MAX='60'               # nombre maximum de sauvegardes de base de données à conserver
PRE_DEPLOY_CUSTOM_ACTIONS='true'         # exécuter le script nommé 'pre-deploy-custom-actions.sh' s’il existe à la racine de la boîte à outils
GIT_UPDATE='true'                        # mettre à jour le projet via git
GIT_SUBMODULE_UPDATE='true'              # mettre à jour les modules git
GIT_SUBMODULE_UPDATE_FROM_REMOTE='false' # mettre à jour les modules git vers la dernière version disponible sur leur dépôt d’origine
COMPOSER_INSTALL='true'                  # installer les dépendances du projet via composer
ARTISAN_MIGRATE='true'                   # mettre à jour la structure de la base de données via artisan
ARTISAN_REFRESH='false'                  # reconstruire la structure de la base de données via artisan (les données seront perdues)
ARTISAN_SEED='false'                     # remplire la base de données avec les jeux de données définis par les seeds
NPM_INSTALL='true'                       # installer les dépendances du projet via npm
NPM_BUILD='true'                         # construire les assets du projet via npm
COMPASS_BUILD='false'                    # construire les assets du projet via compass
GULP_BUILD='true'                        # construire les assets du projet via gulp
PM2_RESTART='false'                      # redémarrer de l’instance Node.js du projet avec PM2
POST_DEPLOY_CUSTOM_ACTIONS='true'        # exécuter le script nommé 'post-deploy-custom-actions.sh' s’il existe à la racine de la boîte à outils
```

## Utilisation

Depuis la racine du projet :
```
./ywd_deploy/deploy.sh
```

Via SSH :
```
ssh utilisateur@hôte /chemin/du/projet/ywd_deploy/deploy.sh
```
