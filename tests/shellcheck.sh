#!/bin/sh
set -o errexit

ROOT_DIR="$(readlink --canonicalize "$(dirname "$0")/..")"

(
	cd "$ROOT_DIR"
	make
	for shell in sh bash dash ksh; do
		printf 'Testing code syntax in %s mode…\n' "$shell"
		shellcheck --shell="$shell" tests/*.sh
		shellcheck --external-sources --shell="$shell" lib/*.sh
		shellcheck --external-sources --shell="$shell" ./*.sh
	done
)

exit 0
