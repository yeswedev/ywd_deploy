prestashop_npm_install() {
	check_dependencies 'find' 'npm'
	find 'themes' -mindepth 3 -maxdepth 3 -type f -name 'package.json' | while read -r file; do
		(
			cd "$(dirname "$file")" || return 1
			check_files 'package.json'
			# shellcheck disable=SC2046
			npm $(npm_install_command)
		)
	done
}
prestashop_npm_build() {
	check_dependencies 'find' 'npm'
	find 'themes' -mindepth 3 -maxdepth 3 -type f -name 'package.json' | while read -r file; do
		(
			cd "$(dirname "$file")" || return 1
			check_directories 'node_modules'
			npm run build
		)
	done
}
prestashop16_compass_install() {
	check_dependencies 'gem'
	find 'themes' -mindepth 2 -maxdepth 2 -type f -name 'config.rb' | while read -r file; do
		(
			cd "$(dirname "$file")" || return 1
			check_files 'config.rb'
			gem install --user-install compass
		)
	done
	return 0
}
prestashop16_compass_build() {
	find 'themes' -mindepth 2 -maxdepth 2 -type f -name 'config.rb' | while read -r file; do
		(
			cd "$(dirname "$file")" || return 1
			check_files 'config.rb'
			if ! command -v compass >/dev/null 2>&1; then
				(
					cd "$PROJECT_PATH" || return 1
					compass_install
				)
			fi
			check_dependencies 'compass'
			check_files 'config.rb'
			compass compile
		)
	done
	return 0
}
