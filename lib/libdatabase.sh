database_backup() {
	printf 'Sauvegarde de la base de données, cette étape peut prendre plusieurs minutes…\n'
	# shellcheck disable=SC2039
	local db_backup_path
	# shellcheck disable=SC2153
	db_backup_path="$BACKUP_PATH/database"
	YWD_AUTOSAVE_DIR="$SCRIPT_PATH/ywd_autosave"
	if [ ! -e "$YWD_AUTOSAVE_DIR/Makefile" ]; then
		(
			cd "$SCRIPT_PATH" || {
				printf 'Critical error:\n' >&2
				printf 'Couldnʼt enter the following directory: %s\n' "$SCRIPT_PATH" >&2
				printf 'Please report this issue on our issues tracker: %s\n' \
					'https://framagit.org/yeswedev/ywd_deploy/issues' >&2
				return 1
			}
			git submodule update --init --recursive ywd_monitor >/dev/null
		)
	fi
	if [ ! -e "$YWD_AUTOSAVE_DIR/libautosave.sh" ]; then
		(
			cd "$YWD_AUTOSAVE_DIR" || {
				printf 'Critical error:\n' >&2
				printf 'Couldnʼt enter the following directory: %s\n' "$YWD_AUTOSAVE_DIR" >&2
				printf 'Please report this issue on our issues tracker: %s\n' \
					'https://framagit.org/yeswedev/ywd_deploy/issues' >&2
				return 1
			}
			make
		)
	fi
	export BACKUPS_PATH="$db_backup_path"
	export MAX_DUMPS="$DB_BACKUP_CLEANUP_MAX"
	"$SCRIPT_PATH/ywd_autosave/quicksave.sh" "$PROJECT_PATH"
	printf '\033[1;32mOK\033[0m\n'
	return 0
}
