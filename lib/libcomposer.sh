composer_install() {
	case "$(get_project_tech)" in
		('drupal_composer')
			drupal_composer_install
		;;
		(*)
			check_files 'composer.json'
			check_dependencies 'composer'
			composer_options='--no-interaction'
			if [ "$(get_project_environment)" = 'production' ]; then
				composer_options="$composer_options --no-dev"
			fi
			# shellcheck disable=SC2086
			composer install $composer_options
		;;
	esac
}
