check_dependencies() {
	for dependency in "$@"; do
		if ! command -v "$dependency" 1>/dev/null 2>&1; then
			echo "Erreur : la commande $dependency est introuvable."
			exit 1
		fi
	done
}
check_files() {
	for file in "$@"; do
		if [ ! -f "$file" ]; then
			echo "Erreur : le fichier $file n’existe pas."
			exit 1
		fi
	done
}
check_variables() {
	for name in "$@"; do
		# shellcheck disable=SC2086
		if [ -z "$(eval printf -- '%b' \"\$$name\")" ]; then
			echo "Erreur : la variable $name n’est pas définie."
			exit 1
		fi
	done
}
get_project_tech() {
	if [ -z "$PROJECT_TECH" ]; then
		check_variables 'PROJECT_PATH'
		YWD_MONITOR_DIR="$SCRIPT_PATH/ywd_monitor"
		if [ ! -e "$YWD_MONITOR_DIR/Makefile" ]; then
			(
				cd "$SCRIPT_PATH" || {
					printf 'Critical error:\n' >&2
					printf 'Couldnʼt enter the following directory: %s\n' "$SCRIPT_PATH" >&2
					printf 'Please report this issue on our issues tracker: %s\n' \
						'https://framagit.org/yeswedev/ywd_deploy/issues' >&2
					return 1
				}
				git submodule update --init --recursive ywd_monitor >/dev/null
			)
		fi
		if [ ! -e "$YWD_MONITOR_DIR/libmonitor.sh" ]; then
			(
				cd "$YWD_MONITOR_DIR" || {
					printf 'Critical error:\n' >&2
					printf 'Couldnʼt enter the following directory: %s\n' "$YWD_MONITOR_DIR" >&2
					printf 'Please report this issue on our issues tracker: %s\n' \
						'https://framagit.org/yeswedev/ywd_deploy/issues' >&2
					return 1
				}
				make >/dev/null
			)
		fi
		PROJECT_TECH=$("$YWD_MONITOR_DIR/get-project-type.sh" "$PROJECT_PATH")
		if [ "$DEBUG" = 'true' ]; then
			# shellcheck disable=SC2016
			printf '$PROJECT_TECH set to "%s" using informations from YWD Monitor.\n' "$PROJECT_TECH" >&2
		fi
	fi
	printf '%s' "$PROJECT_TECH"
}
get_project_environment() {
	case "$(get_project_tech)" in
		('laravel')
			SERVER_ENVIRONMENT=$(laravel_get_environment)
		;;
		('wordpress')
			get_environment_from_wordpress
		;;
		(*)
			# Nothing to do here
			true
		;;
	esac
	if [ -z "$SERVER_ENVIRONMENT" ]; then
		SERVER_ENVIRONMENT='production'
	fi
	printf '%s' "$SERVER_ENVIRONMENT"
	return 0
}
