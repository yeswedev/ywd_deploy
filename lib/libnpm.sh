npm_action() {
	check_files 'package.json'
	check_dependencies 'npm'
	# shellcheck disable=SC2039
	local action
	action="$1"
	# shellcheck disable=SC2086
	npm $action
	return 0
}
npm_install() {
	printf 'Installation des paquets npm, cette étape peut prendre plusieurs minutes…\n'
	case "$PROJECT_TECH" in
		('drupal_composer')
			drupal_npm_install
		;;
		('laravel')
			laravel_npm_install
		;;
		('prestashop')
			prestashop_npm_install
		;;
		('wordpress'*)
			npm_install_wordpress
		;;
		(*)
			check_files 'package.json'
			check_dependencies 'npm'
			# shellcheck disable=SC2046
			npm $(npm_install_command)
		;;
	esac
	printf '\033[1;32mOK\033[0m\n'
}
npm_install_command() {
	# shellcheck disable=SC2039
	local version version_major version_minor install_command
	version=$(npm --version)
	version_major=$(printf '%s' "$version" | cut --delimiter='.' --fields=1)
	version_minor=$(printf '%s' "$version" | cut --delimiter='.' --fields=2)
	# shellcheck disable=SC2086
	if
		{
			[ $version_major -gt 5 ] ||\
			[ $version_major -eq 5 ] && [ $version_minor -ge 7 ]
		} && {
			[ -f 'package-lock.json' ] ||\
			[ -f 'npm-shrinkwrap.json' ]
		}
	then
		install_command='ci'
	else
		install_command='install'
	fi
	printf '%s' "$install_command"
}
npm_build() {
	case "$PROJECT_TECH" in
		('drupal_composer')
			drupal_npm_build
		;;
		('laravel')
			laravel_npm_build
		;;
		('prestashop')
			prestashop_npm_build
		;;
		('wordpress'*)
			npm_build_wordpress
		;;
		(*)
			check_dependencies 'npm'
			get_environment
			if [ "$SERVER_ENVIRONMENT" = 'production' ]; then
				npm_environment='prod'
			else
				npm_environment='dev'
			fi
			npm run $npm_environment
		;;
	esac
}
