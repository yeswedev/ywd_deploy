check_directories() {
	for directory in "$@"; do
		if [ ! -d "$directory" ]; then
			echo "Erreur : le répertoire $directory n’existe pas."
			exit 1
		fi
	done
}
get_environment() {
	SERVER_ENVIRONMENT=$(get_project_environment)
	if [ "$DEBUG" = 'true' ]; then
		printf 'SERVER_ENVIRONMENT=%s\n' "$SERVER_ENVIRONMENT"
	fi
	export SERVER_ENVIRONMENT
	return 0
}
git_update() {
	check_directories '.git'
	check_variables 'BACKUP_PATH'
	[ -d "$BACKUP_PATH" ] || mkdir --parents "$BACKUP_PATH"
	check_dependencies 'git'
	git_update_log="$BACKUP_PATH/git.log"
	date="$(date +%F-%H:%M)"
	commit="$(git show --format=format:%H --no-patch HEAD)"
	printf '%s - HEAD before update: %s\n' "$date" "$commit" >> "$git_update_log"
	git checkout -- .
	git pull
	find . -maxdepth 1 -iname 'readme.md' -delete
	commit="$(git show --format=format:%H --no-patch HEAD)"
	printf '%s - HEAD after update: %s\n' "$date" "$commit" >> "$git_update_log"
	printf -- '%s\n' '-----' >> "$git_update_log"
}
git_submodule_update() {
	check_directories '.git/modules'
	check_dependencies 'git'
	unset options
	if [ "$GIT_SUBMODULE_UPDATE_FROM_REMOTE" = 'true' ]; then
		options='--remote'
	fi
	git submodule update $options
}
gulp_build() {
	if [ -d './node_modules/.bin' ]; then
		export PATH="./node_modules/.bin:$PATH"
	fi
	check_dependencies 'gulp'
	[ "$DEBUG" = 'true' ] && echo "Utilisation de $(command -v gulp)"
	gulp build
}
pm2_restart() {
	check_dependencies 'pm2'
	pm2 restart "$PROJECT_NAME"
}
pre_deploy_actions() {
	check_variables 'PROJECT_TECH' 'PRE_DEPLOY_CUSTOM_ACTIONS' 'SCRIPT_PATH'
	case "$PROJECT_TECH" in
		('laravel')
			laravel_pre_deploy_actions
		;;
		('drupal_composer')
			drupal_pre_deploy_actions
		;;
		(*)
			# Nothing to do here
			true
		;;
	esac
	if [ "$PRE_DEPLOY_CUSTOM_ACTIONS" = 'true' ] && [ -f "$SCRIPT_PATH/pre-deploy-custom-actions.sh" ]; then
		if [ -x "$SCRIPT_PATH/pre-deploy-custom-actions.sh" ]; then
			"$SCRIPT_PATH/pre-deploy-custom-actions.sh"
		else
			sh "$SCRIPT_PATH/pre-deploy-custom-actions.sh"
		fi
	fi
	return 0
}
post_deploy_actions() {
	check_variables 'PROJECT_TECH' 'POST_DEPLOY_CUSTOM_ACTIONS' 'SCRIPT_PATH'
	if [ "$POST_DEPLOY_CUSTOM_ACTIONS" = 'true' ] && [ -f "$SCRIPT_PATH/post-deploy-custom-actions.sh" ]; then
		if [ -x "$SCRIPT_PATH/post-deploy-custom-actions.sh" ]; then
			"$SCRIPT_PATH/post-deploy-custom-actions.sh"
		else
			sh "$SCRIPT_PATH/post-deploy-custom-actions.sh"
		fi
	fi
	case "$PROJECT_TECH" in
		('laravel')
			laravel_post_deploy_actions
		;;
		('drupal_composer')
			drupal_post_deploy_actions
		;;
		(*)
			# Nothing to do here
			true
		;;
	esac
	return 0
}
