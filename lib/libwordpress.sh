get_environment_from_wordpress() {
	check_files '.env'
	check_variables 'PROJECT_PATH'
	SERVER_ENVIRONMENT="$(grep '^WP_ENV=' "$PROJECT_PATH/.env" | sed 's/^WP_ENV=\(.\+\)/\1/')"
	export SERVER_ENVIRONMENT
	return 0
}
npm_install_wordpress() {
	check_variables 'PROJECT_TECH'
	check_dependencies 'find' 'npm'
	# shellcheck disable=SC2039
	local file directory themes_path
	case "$PROJECT_TECH" in
		('wordpress_vanilla')
			themes_path='wp-content/themes'
		;;
		(*)
			themes_path='web/app/themes'
		;;
	esac
	find "$themes_path" -mindepth 2 -maxdepth 2 -type f -name 'package.json' | while read -r file; do
		(
			directory="$(dirname "$file")"
			# shellcheck disable=SC2164
			cd "$directory"
			check_files 'package.json'
			# shellcheck disable=SC2046
			npm $(npm_install_command)
		)
	done
	return 0
}
npm_build_wordpress() {
	check_variables 'PROJECT_TECH'
	check_dependencies 'find' 'npm'
	# shellcheck disable=SC2039
	local file directory themes_path npm_action
	if [ "$(get_project_environment)" = 'production' ]; then
		npm_action='build:production'
	else
		npm_action='build'
	fi
	case "$PROJECT_TECH" in
		('wordpress_vanilla')
			themes_path='wp-content/themes'
		;;
		(*)
			themes_path='web/app/themes'
		;;
	esac
	find "$themes_path" -mindepth 2 -maxdepth 2 -type f -name 'package.json' | while read -r file; do
		(
			directory="$(dirname "$file")"
			# shellcheck disable=SC2164
			cd "$directory"
			check_directories 'node_modules'
			npm run $npm_action
		)
	done
	return 0
}
