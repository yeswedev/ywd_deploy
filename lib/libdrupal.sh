drush_action() {
	if ! PATH="vendor/bin:$PATH" command -v drush >/dev/null; then
		composer_install
	fi
	# shellcheck disable=SC2039
	local action uri
	action="$1"
	if drupal_has_multiples_sites; then
		for uri in $(drupal_get_uris_list); do
			# shellcheck disable=SC2086
			PATH="vendor/bin:$PATH" drush --uri="$uri" $action
		done
	else
		# shellcheck disable=SC2086
		PATH="vendor/bin:$PATH" drush $action
	fi
	return 0
}
drush_maintenance_on() {
	drush_action 'state-set system.maintenance_mode 1'
	return 0
}
drush_maintenance_off() {
	drush_action 'state-set system.maintenance_mode 0'
	return 0
}
drush_cache_rebuild() {
	drush_action 'cache-rebuild'
	return 0
}
drupal_pre_deploy_actions() {
	drush_maintenance_on
	return 0
}
drupal_post_deploy_actions() {
	drush_cache_rebuild
	drush_maintenance_off
	return 0
}
drupal_composer_install() {
	check_dependencies 'find' 'composer'
	if [ -f 'composer.json' ]; then
		composer_options='--no-interaction'
		if [ "$(get_project_environment)" = 'production' ]; then
			composer_options="$composer_options --no-dev"
		fi
		# shellcheck disable=SC2086
		composer install $composer_options
	fi
	# shellcheck disable=SC2039
	local file directory themes_path
	for themes_path in 'web/themes' 'web/themes/contrib' 'web/themes/custom'; do
		if [ -e "$themes_path" ]; then
			while read -r file; do
				if [ ! -f "$file" ]; then
					continue
				fi
				(
					directory="$(dirname "$file")"
					# shellcheck disable=SC2164
					cd "$directory"
					check_files 'composer.json'
					# shellcheck disable=SC2046
					composer install --no-interaction --no-dev
				)
			done <<- EOL
			$(find "$themes_path" -mindepth 2 -maxdepth 2 -type f -name 'composer.json')
			EOL
		fi
	done
}
drupal_npm_install() {
	check_dependencies 'find' 'npm'
	# shellcheck disable=SC2039
	local file directory themes_path
	for themes_path in 'web/themes' 'web/themes/contrib' 'web/themes/custom'; do
		if [ -e "$themes_path" ]; then
			while read -r file; do
				if [ ! -f "$file" ]; then
					continue
				fi
				(
					directory="$(dirname "$file")"
					# shellcheck disable=SC2164
					cd "$directory"
					check_files 'package.json'
					# shellcheck disable=SC2046
					npm $(npm_install_command)
				)
			done <<- EOL
			$(find "$themes_path" -mindepth 2 -maxdepth 2 -type f -name 'package.json')
			EOL
		fi
	done
}
drupal_npm_build() {
	check_dependencies 'find' 'npm'
	# shellcheck disable=SC2039
	local file directory themes_path
	for themes_path in 'web/themes' 'web/themes/contrib' 'web/themes/custom'; do
		if [ -e "$themes_path" ]; then
			while read -r file; do
				if [ ! -f "$file" ]; then
					continue
				fi
				(
					directory="$(dirname "$file")"
					# shellcheck disable=SC2164
					cd "$directory"
					check_directories 'node_modules'
					npm run gulp
				)
			done <<- EOL
			$(find "$themes_path" -mindepth 2 -maxdepth 2 -type f -name 'package.json')
			EOL
		fi
	done
}
drupal_has_multiples_sites() {
	if [ ! -e "$PROJECT_PATH/web/sites/sites.php" ]; then
		return 1
	fi
	test "$(grep --count "^\\s*\$sites\\['" "$PROJECT_PATH/web/sites/sites.php")" -gt 1
}
drupal_get_uris_list() {
	# shellcheck disable=SC2039
	local sites
	sites=$(sed --quiet "/^\\s*\$sites\\['/s/^\\s*\$sites\\['\\(.*\\)'\\] = .*/\\1/p" "$PROJECT_PATH/web/sites/sites.php")
	printf '%s' "$sites"
	return 0
}
