laravel_get_environment() {
	check_files '.env'
	# shellcheck disable=SC2039
	local server_environment
	server_environment="$(grep '^APP_ENV=' .env | sed 's/^APP_ENV=\(.\+\)/\1/')"
	printf '%s' "$server_environment"
	return 0
}
laravel_npm_install() {
	npm_action "$(npm_install_command)"
	return 0
}
laravel_npm_build() {
	check_directories 'node_modules'
	get_environment
	# shellcheck disable=SC2153
	if [ "$SERVER_ENVIRONMENT" = 'production' ]; then
		npm_environment='prod'
	else
		npm_environment='dev'
	fi
	npm_action "run $npm_environment"
}
laravel_pre_deploy_actions() {
	artisan_maintenance_on
	return 0
}
laravel_post_deploy_actions() {
	artisan_maintenance_off
	return 0
}
artisan_action() {
	check_files 'artisan'
	check_dependencies 'php'
	# shellcheck disable=SC2039
	local action
	action="$1"
	# shellcheck disable=SC2086
	php artisan $action
	return 0
}
artisan_migration() {
	artisan_action 'migrate --force'
	return 0
}
artisan_refresh() {
	artisan_action 'migrate:refresh --force'
	return 0
}
artisan_seed() {
	artisan_action 'db:seed --force'
	return 0
}
artisan_maintenance_on() {
	artisan_action 'down'
	return 0
}
artisan_maintenance_off() {
	artisan_action 'up'
	return 0
}
