compass_install() {
	check_dependencies 'gem'
	check_variables 'PROJECT_TECH'
	case "$PROJECT_TECH" in
		('prestashop1.6')
			prestashop16_compass_install
		;;
		(*)
			gem install --user-install compass
		;;
	esac
	return 0
}
compass_build() {
	check_variables 'PROJECT_TECH'
	case "$PROJECT_TECH" in
		('prestashop1.6')
			prestashop16_compass_build
		;;
		(*)
			if ! command -v compass >/dev/null 2>&1; then
				compass_install
			fi
			check_dependencies 'compass'
			check_files 'config.rb'
			compass compile
		;;
	esac
	return 0
}
