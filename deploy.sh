#!/bin/sh
set -e

SCRIPT_PATH="$(dirname "$(readlink --canonicalize "$0")")"
export PATH="$PATH:$SCRIPT_PATH/bin"

if command -v make >/dev/null 2>&1; then
	(
		cd "$SCRIPT_PATH"
		make
	)
fi
# shellcheck source=libdeploy.sh
. "$SCRIPT_PATH/libdeploy.sh"
check_files "$SCRIPT_PATH/toolbox.conf"
# shellcheck source=toolbox.conf.example
. "$SCRIPT_PATH/toolbox.conf"

export PROJECT_PATH
cd "$PROJECT_PATH"

pre_deploy_actions

if [ "$DB_BACKUP" = 'true' ]; then
	database_backup
fi

if [ "$GIT_UPDATE" = 'true' ] && [ -d '.git' ]; then
	git_update
fi

if [ "$GIT_SUBMODULE_UPDATE" = 'true' ] && [ -d '.git/modules' ]; then
	git_submodule_update
fi

if [ "$COMPOSER_INSTALL" = 'true' ]; then
	case "$(get_project_tech)" in
		('drupal_composer')
			composer_install
		;;
		(*)
			if [ -f 'composer.json' ]; then
				composer_install
			fi
		;;
	esac
fi

if [ "$ARTISAN_MIGRATE" = 'true' ] && [ -f 'artisan' ]; then
	artisan_migration
fi

if [ "$ARTISAN_REFRESH" = 'true' ] && [ -f 'artisan' ]; then
	artisan_refresh
fi

if [ "$ARTISAN_SEED" = 'true' ] && [ -f 'artisan' ]; then
	artisan_seed
fi

if [ "$NPM_INSTALL" = 'true' ]; then
	case "$PROJECT_TECH" in
		('drupal_composer'|'laravel'|'prestashop'|'wordpress'*)
			npm_install
		;;
		(*)
			if [ -f 'package.json' ]; then
				npm_install
			fi
		;;
	esac
fi

if [ "$NPM_BUILD" = 'true' ]; then
	case "$PROJECT_TECH" in
		('drupal_composer'|'laravel'|'prestashop'|'wordpress'*)
			npm_build
		;;
		(*)
			if [ -d 'node_modules' ]; then
				npm_build
			fi
		;;
	esac
fi

if [ "$COMPASS_BUILD" = 'true' ]; then
	case "$PROJECT_TECH" in
		('prestashop1.6')
			compass_build
		;;
		(*)
			if [ -f 'config.rb' ]; then
				compass_build
			fi
		;;
	esac
fi

if [ "$GULP_BUILD" = 'true' ] && [ -f 'gulpfile.js' ]; then
	gulp_build
fi

if [ "$PM2_RESTART" = 'true' ] && command -v 'pm2' 1>/dev/null 2>&1; then
	pm2_restart
fi

post_deploy_actions

exit 0
