all: libdeploy.sh

libdeploy.sh: lib/*
	cat lib/* > libdeploy.sh

clean:
	rm -f libdeploy.sh
